# Poly2SVG
Convert poly to SVG

**Usage:**
* Put *.poly files into POLY directory
    * To obtain POLY files:
    ```
    ./get_poly.sh zipname country_code [level_from level_to]
    ```
* Launch generate.php script 
    * To execute from terminal: 
    ```
    php generate.php > output.svg
    ```


POLY files can be exported from [osm.wno-edv-service.de](https://osm.wno-edv-service.de/boundaries/)

Full documentation [here](https://osm.wno-edv-service.de/index.php/projekte/internationale-administrative-grenzen/boundaries-map-4-1-english-version)
