# SVG to JVectorMap conversion 

* Replace, in *.svg file, "id" parameter with "name" parameter
* Copy SVG code in [svg.wangxingrong.com](http://svg.wangxingrong.com/) and check result
* Save result code in jvectormap file (Ex: jquery-jvectormap-german-districts.js)
